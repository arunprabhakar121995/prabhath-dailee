import React from "react";

function Name(props){
    return(
        <>
        </>
    );
}

export default Name;




Important Libraries

1. React Hook Form - For Forms
2. bcrypt - For Encryption


Page import in main.jsx

import Names from './routes/Names/names';
import Name from './routes/Names/name';
import AddName from './routes/Names/addName';
import UpdateName from './routes/Names/updatename';
import DeleteName from './routes/Names/deleteName';
import DeleteNameMsg from './routes/Names/deleteNameMsg';


Page children in main.jsx


      {
        path: "/names",
        element: <Names />,
      },
      {
        path: "/names/:nameId",
        element: <Name />,
      },
      {
        path: "/names/addname",
        element: <AddName />
      },
      {
        path: "/names/updatename/:nameId",
        element: <UpdateName />,
        
      },
      {
        path: "/names/deletename/:nameId",
        element: <DeleteName />
      },
      {
        path: "/names/deletename/deletedname",
        element: <DeleteNameMsg />
      },


