import React from "react";
import { Link, useLoaderData } from "react-router-dom";
import axios from "axios";
const DB_URL = import.meta.env.VITE_DB_URL

  export async function loader({params}) {
    const response = await axios.get(`${DB_URL}/authors/${params.authorId}`);
    const author = response.data  //This line is coz axios give data inside data field
    return { author };
  };
function Author(props){

    const { author } = useLoaderData();
    return(
        <>
        <main>
        <section className="container mx-auto flex flex-col lg:grid grid-cols-2 my-16 px-2 md:px-10 gap-20 justify-center items-center text-red-950">
                <img className="flex items-center w-full px-10 rounded-full object-cover drop-shadow-xl" src={author.image} alt="" />
                <div className="flex flex-col mx-auto justify-center bg-red-200 p-5 rounded-xl">
                    <h1 className="text-4xl lg:text-5xl font-bold my-10">{author.name}</h1>                   
                    <span className="py-2 text-xl border-t-2  border-white">{`Occupation : ${author.occupation}`}</span>
                    <span className="py-5 text-2xl border-t-2  border-white">{`Born: ${author.born}`}</span> 
                    {author.birthplace && <span className="py-2 text-xl border-t-2  border-white">{`Birthplace : ${author.birthplace}`}</span> }
                    {author.spouse && <span className="py-2 text-xl border-t-2  border-white">{`Spouse : ${author.spouse}`}</span> }
                    {author.children && <span className="py-2 text-xl border-t-2  border-white">{`Children : ${author.children}`}</span>}
                    <p className="py-5 text-xl border-t-2  border-white">{author.about}</p>
                </div>
            </section>
        </main>
        </>
    )
}

export default Author;