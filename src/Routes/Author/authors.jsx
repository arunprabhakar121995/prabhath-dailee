import React from "react";
import { Link,useLoaderData } from "react-router-dom";
import AuthorCard from "../../Components/Cards/authorCard";
import axios from "axios";
const DB_URL = import.meta.env.VITE_DB_URL

  export async function loader() {
    const response = await axios.get(`${DB_URL}/authors`);
    const authors = response.data  //This line is coz axios give data inside data field
    return { authors };
  }
function Authors(props){
    const { authors } = useLoaderData();
    return(
        <>
        <main className="my-10">
        <section className="">
            <div className="container mx-auto">
                <h2 className="text-5xl pt-5 px-10 font-bold font-sanserif">Authors</h2>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5">
                    {
                        authors.map(author => {
                            return(
                                <AuthorCard key={authors._id} author={author} />
                            )
                        })
                    }                        
                </div>
            </div>
        </section>
        </main>
        </>
    );
}

export default Authors;