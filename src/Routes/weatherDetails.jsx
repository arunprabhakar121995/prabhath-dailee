import React, { useEffect, useState } from 'react'
import axios from 'axios';
import CurrentWeather from '../Components/WeatherPageComponents/currentWeather'
import WeatherForecast from '../Components/WeatherPageComponents/WeatherForecast'
import TemperatureGraph from '../Components/WeatherPageComponents/TemperatureGraph'
import PrecipitationChart from '../Components/WeatherPageComponents/PrecipitationChart'
import WindSpeedDirection from '../Components/WeatherPageComponents/windSpeedDirection';
import Loading from '../Components/loading';

const Weather_API_KEY = import.meta.env.VITE_WEATHER_API_KEY

export default function WeatherPage() {
    const [weatherData, setWeatherData] = useState(null)
    const [forecastData, setForecastData] = useState(null)


    useEffect(() => {
      const fetchWeatherData = async () => {
          try {
              // Get user's current position
              navigator.geolocation.getCurrentPosition(async (position) => {
                  const latitude = position.coords.latitude
                  const longitude = position.coords.longitude

                  // Fetch current weather data
                  const currentResponse = await axios.get(`https://api.weatherapi.com/v1/current.json?q=${latitude},${longitude}&key=${Weather_API_KEY}`)
                  setWeatherData(currentResponse.data);

                  // Fetch 5-day forecast data
                  const forecastResponse = await axios.get(`https://api.weatherapi.com/v1/forecast.json?q=${latitude},${longitude}&days=5&key=${Weather_API_KEY}`)
                  setForecastData(forecastResponse.data)
              });
          } catch (error) {
              console.error('Error fetching weather data:', error)
          }
      };

      fetchWeatherData()
  }, [])

  if (!weatherData || !forecastData) {
    return <Loading />;
  }

    return (
        <main className="bg-red-50">
                    <div className="container mx-auto p-4 ">
            <header className="text-center mb-8">
                <h1 className="text-3xl font-bold">Weather Forecast</h1>
                <p>{weatherData.location.name}, {weatherData.location.country}</p>
            </header>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                <CurrentWeather data={weatherData.current} />
                <WindSpeedDirection data={weatherData.current} />
                <div className="col-span-2">
                    <WeatherForecast data={forecastData.forecast.forecastday} />
                </div>
                <div className="col-span-2">
                    <TemperatureGraph data={forecastData.forecast.forecastday} />
                </div>
                <div className="col-span-2">
                    <PrecipitationChart data={forecastData.forecast.forecastday} />
                </div>
            </div>
            
        </div>
        </main>
        
        
    );
}
