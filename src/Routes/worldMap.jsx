import React from 'react';
import Map from '../Components/map';

function WorldMap(){
  return(
    <main>
        <section className='container m-auto p-2 md:p-10'>
          <h1 className="text-red-950 text-6xl py-10">World Map</h1>
          <Map />
        </section>
    </main>
  )
}

export default WorldMap;