import React from "react";
import { Link } from "react-router-dom";
import SignUpForm from "../../Components/Forms/sigupForm";
import LinkButton from "../../Components/Buttons/linkButton";


function SignUp(props){
    return(
        <>
        <main className="container px-5 flex flex-col m-auto max-w-screen-md">
            <section>
                <h2 className="text-6xl text-red-950 my-10">SIGNUP</h2>
                <SignUpForm />
                <div className="flex flex-col items-center justify-between container m-auto">
                    <span>If you already have an account</span>
                    <LinkButton path="/login" buttonName="Login" color="red" />
                </div>
            </section>
        </main>
        </>
    );
}

export default SignUp;