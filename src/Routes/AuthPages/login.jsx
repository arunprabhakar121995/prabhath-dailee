import React from "react";
import LinkButton from "../../Components/Buttons/linkButton";
import LoginForm from "../../Components/Forms/loginForm";


function Login(props){
    return(
        <>
        <main className="container px-5 flex flex-col m-auto max-w-screen-md">
            <section>
                <h2 className="text-6xl text-red-950 my-10">LOGIN</h2>
                <LoginForm />
                <div className="flex flex-col items-center justify-between container m-auto">
                    <span>If you don't have an Account</span>
                    <LinkButton path="/signup" buttonName="SignUp" color="red" />
                </div>
            </section>
        </main>
        </>
    );
}

export default Login;