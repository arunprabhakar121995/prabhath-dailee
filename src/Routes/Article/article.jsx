import React from "react";
import axios from "axios";
import { Link, useLoaderData } from "react-router-dom";

  export async function loader({params}) {
    const DB_URL = import.meta.env.VITE_DB_URL;
    const response = await axios.get(`${DB_URL}/articles/${params.articleId}`);
    const article = response.data  //This line is coz axios give data inside data field
    return { article }
  }
function Article(props){
  const article = useLoaderData();
  console.log(article)
    return(
        <>
        <main className="bg-red-50">
          <section className="container mx-auto flex flex-col my-16 px-2 md:px-10 gap-20 justify-center items-center">
                <div className="flex flex-col justify-center bg-red-900 p-5 rounded-xl ">
                    <h1 className="text-4xl text-red-200 font-bold my-10">{article.article.title}</h1>
                    <img className="flex items-center px-10 py-5 lg:px-24 rounded-lg" src={article.article.image ? article.article.image : "https://talentclick.com/wp-content/uploads/2021/08/placeholder-image.png"} 
                    alt={article.article.title}  />
                    <span className="text-red-200">{`Published : ${article.article.date}`}</span>
                    <p className="my-5 text-xl text-red-200">{article.article.description}</p>
                </div>
            </section>
        </main>
        </>
    )
}

export default Article;