import React from "react";
import axios from "axios";
import { useLoaderData } from "react-router-dom";
import MainNews from "../../Components/Cards/mainHomeNews";
import SideKicks from "../../Components/Cards/sidekicks";

export async function loader() {  
  const DB_URL = import.meta.env.VITE_DB_URL
  const response = await axios.get(`${DB_URL}/articles`)
  const articles = (response.data)
  return { articles};
}
function Articles(props){
  const { articles } = useLoaderData();

  // Select the first article
  const mainArticle = articles.length > 0 ? articles[0] : null;
  const sidekicks = articles.slice(1)
    return(
      <main className="bg-red-50">
        <div className="container m-auto">
          <section className="section1 py-20">
            <h1 className="text-6xl mb-10">Happening NOW!!!</h1>
              <div id="articlediv" className="grid gap-5 mx-2 lg:grid-cols-2 lg:grid-rows-4 lg:gap-8 xl:gap-16">
                {mainArticle && <MainNews article={mainArticle} />}

                {sidekicks.map((article, index) => (
                  <SideKicks key={index} article={article} />
                ))}
              </div>
      
          </section>
        </div>
      </main>
  );
}

export default Articles;