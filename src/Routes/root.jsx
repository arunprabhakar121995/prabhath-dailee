import { Outlet } from "react-router-dom";
import Headertop from "../Components/headertop";
import HeaderBottom from "../Components/headerBottom";
import { useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { changeLoginStatus } from "../features/login/loginSlice";

export default function Root() {
  const DB_URL = import.meta.env.VITE_DB_URL;
  const loggedIn = useSelector(state => state.login.loggedIn)
  const dispatch = useDispatch()

  useEffect(() => {
      axios.get(`${DB_URL}/auth/verify`, { withCredentials:true })
          .then(response => {
              dispatch(changeLoginStatus(true))
          })
          .catch(error => {
              dispatch(changeLoginStatus(false))
          })
  }, [dispatch])
    return (
      <>
      <header className="bg-red-50">
        <div className="container mx-auto px-5 my-2">
          <Headertop />
          <HeaderBottom />
        </div>
      </header>
        <Outlet />
        <footer>
        </footer>
      </>
    )
  }