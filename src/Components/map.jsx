import React, { useRef, useEffect, useState } from 'react';
import maplibregl from 'maplibre-gl';
import { GeocodingControl } from "@maptiler/geocoding-control/react";
import { createMapLibreGlMapController } from "@maptiler/geocoding-control/maplibregl-controller";
import "@maptiler/geocoding-control/style.css";
import 'maplibre-gl/dist/maplibre-gl.css';
import Loading from './loading';

export default function Map() {
  const mapContainer = useRef(null);
  const map = useRef(null);  
  const [location, setLocation] = useState({ latitude: 0, longitude: 0 });
  const [zoom] = useState(14);
  const VITE_MAP_API_KEY = import.meta.env.VITE_MAP_API_KEY;
  const [API_KEY] = useState(`${VITE_MAP_API_KEY}`);
  const [mapController, setMapController] = useState();
  const [loadingData, setLoadingData] = useState(true); // Add loading state

  useEffect(() => {
    // Fetch user's current location
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setLocation({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
        },
        (error) => {
          console.error("Error fetching geolocation: ", error);
          // Set a default location if geolocation fails (optional)
          setLocation({ latitude: 37.7749, longitude: -122.4194 }); // San Francisco, for example
        }
      );
    } else {
      console.error("Geolocation is not supported by this browser.");
      // Set a default location if geolocation is not supported
      setLocation({ latitude: 37.7749, longitude: -122.4194 }); // San Francisco, for example
    }
  }, []);

  useEffect(() => {
    if (map.current) return; // stops map from intializing more than once

    map.current = new maplibregl.Map({
      container: mapContainer.current,
      style: `https://api.maptiler.com/maps/streets-v2/style.json?key=${API_KEY}`,
      center: [location.longitude, location.latitude],
      zoom: zoom
    });

    map.current.addControl(new maplibregl.NavigationControl(), 'top-right');
    setMapController(createMapLibreGlMapController(map.current, maplibregl));

    map.current.on('load', () => {
      setLoadingData(false); // To set loading to false once the map data is loaded
    });
  }, [API_KEY, location, zoom]);

  useEffect(() => {
    if (map.current && location.latitude !== 0 && location.longitude !== 0) {
      map.current.setCenter([location.longitude, location.latitude]);
    }
  }, [location]);

  return (
    <div className="map-wrap">
      {loadingData && (<Loading />)}
      <div className="geocoding">
        <GeocodingControl apiKey={API_KEY} mapController={mapController} />
      </div>
      <div ref={mapContainer} className="map rounded-2xl" />
    </div>
  );
}
