import { useForm } from "react-hook-form"
import axios from "axios"
import { useState } from "react"
import InputSubmitButton from "../Buttons/inputSubmitButton"
import { useNavigate } from "react-router-dom"

export default function SignUpForm() {
  const [showPassword, setShowPassword] = useState(false)
  const [showConfirmPassword, setShowConfirmPassword] = useState(false)

  const {
    register, 
    handleSubmit, 
    watch,
    formState: { errors },
  } = useForm()

  const DB_URL = import.meta.env.VITE_DB_URL
  const navigate = useNavigate()

  const onSubmit = (data) => {
      // Remove confirmPassword from data object
  const { confirmPassword, ...formData } = data;
    axios.post(`${DB_URL}/users`, formData)
      .then(response => {
        const username = formData.name
        window.alert(`Hello ${username}, Welcome to Prabhath Dailee`)
        navigate("/login")
      })
      .catch(error => console.error('Error:', error))
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-5">
      <label htmlFor="name">Name:</label>
      <input 
        type="text" 
        id="name" 
        placeholder="Type Your Name" 
        className="border border-gray-900 p-2" 
        {...register("name", { required: true, maxLength: 25 })} 
      />
      {errors.name && <span className="text-red-600">Please Check Your Name</span>}

      <label htmlFor="email">E-Mail:</label>
      <input 
        type="email" 
        id="email" 
        placeholder="Type Your E-Mail" 
        className="border border-gray-900 p-2" 
        {...register("email", { required: true, pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/ })} 
      />
      {errors.email && <span className="text-red-600">Invalid E-Mail</span>}

      <label htmlFor="password">Password:</label>
      <div className="relative">
        <input 
          type={showPassword ? "text" : "password"} 
          id="password" 
          placeholder="Type Your Password" 
          className="border border-gray-900 p-2 w-full" 
          {...register("password", { required: true, pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.])[A-Za-z\d@$!%*?&.]{8,}$/ })} 
        />
        <span 
          className="absolute right-2 top-2 cursor-pointer" 
          onClick={() => setShowPassword(!showPassword)}
        >
          {showPassword ? "Hide" : "Show"}
        </span>
      </div>
      {errors.password && <span className="text-red-600">Your Password should contain at least 8 characters, one uppercase, one lowercase, one digit, one special character</span>}

      <label htmlFor="confirmPassword">Confirm Password:</label>
      <div className="relative">
        <input 
          type={showConfirmPassword ? "text" : "password"} 
          id="confirmPassword" 
          placeholder="Confirm Your Password" 
          className="border border-gray-900 p-2 w-full" 
          {...register("confirmPassword", { 
            required: true, 
            validate: value => value === watch('password') || "Passwords does not match"
          })} 
        />
        {errors.confirmPassword && <span className="text-red-600">{errors.confirmPassword.message}</span>}
        <span 
          className="absolute right-2 top-2 cursor-pointer" 
          onClick={() => setShowConfirmPassword(!showConfirmPassword)}
        >
          {showConfirmPassword ? "Hide" : "Show"}
        </span>
      </div>

      <InputSubmitButton color="red" buttonName="Submit" />
    </form>
  )
}
