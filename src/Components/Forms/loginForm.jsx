import { useForm } from "react-hook-form"
import InputSubmitButton from "../Buttons/inputSubmitButton"
import { useState } from "react"
import { useNavigate } from "react-router-dom"
import axios from "axios"
import { useDispatch } from "react-redux"
import { changeLoginStatus } from "../../features/login/loginSlice"

const DB_URL = import.meta.env.VITE_DB_URL


export default function LoginForm() {
    const navigate= useNavigate()
    const dispatch= useDispatch()
    const [showPassword, setShowPassword] = useState(false)
  const {
    register, 
    handleSubmit, 
    watch,
    formState: { errors },
  } = useForm()


  const onSubmit = (data) => { 
    axios.post(`${DB_URL}/auth/login`, data, {withCredentials: true}) //To let axios know we use cookies
    .then(response => {
      dispatch(changeLoginStatus(true))
      window.alert("Logged In")
      navigate("/")
  })
    
    .catch(error => {
      dispatch(changeLoginStatus(false))
      window.alert("Invalid Credentials")
    })
  }


  console.log(watch("LoggeIn"))


  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-5">

        <label htmlFor="name">E-Mail:</label>
        <input type="email" id="email" placeholder="Type Your E-Mail" className="border border-gray-900 p-2" {...register("email", { required: true, pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/ })} />
        {errors.email && <span className="text-red-600">Invalid E-Mail</span>}

        <label htmlFor="password">Password:</label>
        <div className="relative">
        <input 
          type={showPassword ? "text" : "password"} 
          id="password" 
          placeholder="Type Your Password" 
          className="border border-gray-900 p-2 w-full" 
          {...register("password", { required: true, pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.])[A-Za-z\d@$!%*?&.]{8,}$/ })} 
        />
        <span 
          className="absolute right-2 top-2 cursor-pointer" 
          onClick={() => setShowPassword(!showPassword)}
        >
          {showPassword ? "Hide" : "Show"}
        </span>
      </div>
        <InputSubmitButton color="red" buttonName="Login" />
    </form>
  )
}