import React, { useEffect, useState } from "react";
import "/node_modules/flag-icons/css/flag-icons.min.css";

// Function to store the last refresh time in localStorage
const storeLastRefreshTime = () => {
  const currentTime = new Date();
  localStorage.setItem("lastRefreshTime", currentTime);
};

// Function to retrieve and convert the refresh time to the user's local time
const getLastRefreshTime = (timeZone) => {
  const storedTime = localStorage.getItem("lastRefreshTime");
  if (storedTime) {
    const date = new Date(storedTime);
    return new Intl.DateTimeFormat("en-US", {
      timeZone: timeZone || Intl.DateTimeFormat().resolvedOptions().timeZone,
      hour: "2-digit",
      minute: "2-digit",
    }).format(date);
  }
  return null;
};

// Mapping of country names to time zones
const timeZoneToCountry = {
  "Asia/Calcutta": "India",
  "America/New_York": "USA",
  "Europe/London": "UK",
  "Australia/Sydney": "Australia",
  "Asia/Tokyo": "Japan",
  // Add more time zones and countries as needed
};

// Mapping of countries to country codes
const countryToCountryCode = {
  "India": "in",
  "USA": "us",
  "UK": "gb", // Corrected UK country code to 'gb'
  "Australia": "au",
  "Japan": "jp",
  // Add more countries and their codes as needed
};

const LastRefreshTime = () => {
  const [lastRefreshTime, setLastRefreshTime] = useState("");
  const [Country, setCountry] = useState("");
  const [countryCode, setCountryCode]= useState("");

  useEffect(() => {
    // Store the current refresh time in localStorage
    storeLastRefreshTime();

    // Detect the user's time zone
    const detectedTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    // Map the time zone to a country
    const detectedCountry = timeZoneToCountry[detectedTimeZone] || "Unknown";
    setCountry(detectedCountry);

    // Retrieve and set the last refresh time in the component state
    const lastRefresh = getLastRefreshTime(detectedTimeZone);
    setLastRefreshTime(lastRefresh);

    // Converting country to country code to set flag
    const countryFlag = countryToCountryCode[detectedCountry];
    setCountryCode(countryFlag);
  }, []);

  return (
    <div  className="mx-2 my-2 text-sm">
      <span>
        {lastRefreshTime ? `Updated at: ${lastRefreshTime} ${Country}` : "No refresh time recorded"}          {countryCode && <span className={`fi fi-${countryCode} fis`}></span>}
      </span>
    </div>
  );
};

export default LastRefreshTime;
