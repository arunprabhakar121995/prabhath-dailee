import React from "react";
import { Link } from "react-router-dom";
function AuthorCard(props){
    const author = props.author
    return(
        <>
        <article className="m-5">
                    <Link className="md:h-60 p-3 my-5" to ={`/authors/${author._id}`}>
                    <div className="mx-10 md:mx-0 md:p-2 hover:border-2 hover:border-red-200 shadow-2xl shadow-red-200 hover:shadow-red-600 rounded-xl flex flex-col items-center">
                        <img className="object-cover w-fit h-72 p-2 rounded-xl" src={author.image} alt="" />
                        <div className="flex flex-col mt-3 h-32 p-2">
                            <h3 className="text-2xl font-bold">{author.name}</h3>
                            <span className="text-md pt-2">{author.occupation}</span>
                        </div>
                        </div>
                    </Link>
                </article>
        </>
    );
}

export default AuthorCard;