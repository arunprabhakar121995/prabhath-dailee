import React from "react";
import { Link } from "react-router-dom";

function MainNews(props){
const article = props.article
    return(
        <article className="lg:row-start-1 lg:row-span-4">
            <Link to={`/articles/${article._id}`}>
                <img src={article.image? article.image : "https://talentclick.com/wp-content/uploads/2021/08/placeholder-image.png"} alt="" className="rounded-lg w-full" />
                <div>
                    <h2 className="text-4xl font-bold mt-5 mb-10">{article.title}</h2>
                    <p>{article.description}</p>
                </div>
            </Link>
        </article>
    )
}

export default MainNews