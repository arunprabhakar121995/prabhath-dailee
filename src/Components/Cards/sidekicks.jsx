import React from "react";
import { Link } from "react-router-dom";

function SideKicks(props){
    const article = props.article

    return(
        <article>
            <Link className="grid grid-cols-2 gap-5" to={`/articles/${article._id}`}>
                <img src={article.image? article.image : "https://talentclick.com/wp-content/uploads/2021/08/placeholder-image.png"} alt="" className="rounded-lg" />
                <div>
                    <h2 className="text-lg font-bold mb-2">{article.title}</h2>
                </div>
            </Link>
        </article>
    )
}

export default SideKicks