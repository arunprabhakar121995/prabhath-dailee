import React from "react";
import { Link } from "react-router-dom";


function LinkButton({ path, buttonName, color }){
    const colorClasses = {
        violet: "bg-violet-950 hover:bg-violet-200 text-violet-200 hover:text-violet-950 border-2 border-violet-200 hover:border-violet-950",
        red: "bg-red-950 hover:bg-red-200 text-red-200 hover:text-red-950 border-2 border-red-200 hover:border-red-950",
        blue: "bg-blue-950 hover:bg-blue-200 text-blue-200 hover:text-blue-950 border-2 border-blue-200 hover:border-blue-950",
        green: "bg-green-950 hover:bg-green-200 text-green-200 hover:text-green-950 border-2 border-green-200 hover:border-green-950",
        yellow: "bg-yellow-950 hover:bg-yellow-200 text-yellow-200 hover:text-yellow-950 border-2 border-yellow-200 hover:border-yellow-950"
    };
    return(
        <>
        <Link className={`${colorClasses[color]} m-auto px-10 py-2 my-5 rounded-xl text-xl hover:text-2xl hover:font-bold  transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 duration-300`} to={path}>{buttonName}</Link>
        </>
    );
}

export default LinkButton;