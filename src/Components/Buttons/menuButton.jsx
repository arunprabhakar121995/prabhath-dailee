import React from "react";
import { Link } from "react-router-dom";


function MenuButton(props){

    return(
        <>
        <div className="block lg:hidden dropdown inline-block">
                <button className="justify-center">
                    <span className="material-symbols-outlined text-3xl md:text-5xl text-red-950 hover:text-red-300 ease-in-out duration-300">
                        menu
                    </span>
                </button>
                <div className="content drop-shadow-lg hidden absolute min-w-28 text-red-950 bg-red-400 right-6 rounded-md">
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300 rounded-t-md" to="/">Home</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="/articles">Articles</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="/authors">Authors</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="/contactus">ContactUs</Link>
                        <Link className="block px-10 py-3 text-xl bg-red-300 text-red-950 hover:text-red-300 hover:bg-red-950 ease-in-out duration-300" to="/login">Login</Link>
                    </div>
            </div>
        </>
    )
}

export default MenuButton;