import React from "react";
import CurrentDate from "./currentdate";
import LastRefreshTime from "./lastrefreshtime";
import Weather from "./weather";
import { Link } from "react-router-dom";

function Headertop(props){
    return(
        <>
        <div className="container m-auto border border-red-200 flex align-center justify-between h-10">
            <div className="flex flex-row">
                <CurrentDate />
                <div className="hidden lg:block">
                    <div className="flex flex-row align-center ">
                        <span className="py-2">|</span>
                        <LastRefreshTime />
                    </div>
                </div>
                <Link to="/worldmap">
                    <span className="material-symbols-outlined text-red-950 py-2 px-2 hover:font-bold">
                        globe_asia
                    </span>
                </Link>
            </div>
            <Weather />
        </div>
        </>
    );
}

export default Headertop;