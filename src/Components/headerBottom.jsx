import React from "react";
import { Link } from "react-router-dom";
import Logo from "./componentImages/newsapplogo.png"
import MenuButton from "./Buttons/menuButton";
import Dropdown from "./dropdown";
import { useSelector } from "react-redux";

function HeaderBottom(props){
    const loggedIn = useSelector((state) => state.login.loggedIn)
    return(
        <>
            <div className="flex flex-row items-center justify-between">
                <Link to="/">
                    <div className="flex flex-row items-center">
                        <img className="w-24 md:w-32" src={Logo} alt="" />
                        <h1 className="text-2xl sm:text-4xl font-bold text-red-950 p-5">PRABHATH DAILEE</h1>
                    </div>
                </Link>
                <MenuButton />
                <nav className="hidden lg:block">
                    <ul className="flex flex-row gap-5 text-red-950 text-xl">
                        <li>
                            <Link className="hover:font-bold transition ease-in-out delay-150 hover:animate-pulse hover:text-2xl hover:scale-110 duration-300" to="/">Home</Link>
                        </li>
                        <li>
                        <Link className="hover:font-bold transition ease-in-out delay-150 hover:animate-pulse hover:text-2xl hover:scale-110 duration-300" to="/articles">Articles</Link>
                        </li>
                        <li>
                        <Link className="hover:font-bold transition ease-in-out delay-150 hover:animate-pulse hover:text-2xl hover:scale-110 duration-300" to="/authors">Authors</Link>
                        </li>
                        <li>
                        <Link className="hover:font-bold transition ease-in-out delay-150 hover:animate-pulse hover:text-2xl hover:scale-110 duration-300" to="/contactus">ContactUs</Link>
                        </li>
                        {loggedIn?
                        <li>
                        <Link className="hover:font-bold transition ease-in-out delay-150 hover:animate-pulse hover:text-2xl hover:scale-110 duration-300" to="/logout">Logout</Link>
                        </li>
                        :
                        <li>
                        <Link className="hover:font-bold transition ease-in-out delay-150 hover:animate-pulse hover:text-2xl hover:scale-110 duration-300" to="/login">Login</Link>
                        </li>
                        }
                        
                    </ul>
                </nav>
            </div>
            <Dropdown />
        </>
    );
}

export default HeaderBottom;