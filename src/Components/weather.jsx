import axios from "axios";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function Weather() {
  const [weather, setWeather] = useState(null);
  const [weathericon, setweathericon] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const Weather_API_KEY = import.meta.env.VITE_WEATHER_API_KEY;

    const getWeatherData = async (latitude, longitude) => {
      const API_URL = `https://api.weatherapi.com/v1/current.json?q=${latitude},${longitude}&key=${Weather_API_KEY}`

      try {
        const response = await axios.get(API_URL);
        setWeather(response.data.current.temp_c);
        setweathericon(response.data.current.condition.icon)
      } catch (error) {
        console.error("Error fetching weather data:", error);
      }
    };

    const getGeoLocation = () => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const { latitude, longitude } = position.coords;
            getWeatherData(latitude, longitude);
          },
          (geoError) => {
            console.error("Error getting geolocation:", geoError);
            setError("Error getting geolocation");
          }
        );
      } else {
        console.error("Geolocation is not supported by this browser.");
        setError("Geolocation is not supported by this browser.");
      }
    };

    getGeoLocation();
  }, []);

  return (
    <Link className="flex flex-row mx-2 my-2 text-sm" to="/weather">
      {weathericon && <img src={weathericon} alt="" /> }
      {weather && <span className="text-gray-400">{weather}&#176;C</span>}
    </Link>
  );
}

export default Weather;
