import React from 'react';

const CurrentWeather = ({ data }) => {
    return (
        <div className="p-4 bg-white shadow-md rounded-lg">
            <h2 className="text-xl font-semibold">Current Weather</h2>
            <p>Temperature: {data.temp_c}°C</p>
            <p>Condition: {data.condition.text}</p>
            <img src={data.condition.icon} alt={data.condition.text} />
        </div>
    );
};

export default CurrentWeather;