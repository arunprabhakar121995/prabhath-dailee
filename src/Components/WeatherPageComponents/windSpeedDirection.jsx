import React from 'react';

const WindSpeedDirection = ({ data }) => {

    // Define the direction mapping
const directionMapping = {
    N: 'North',
    NNE: 'North-NorthEast',
    NE: 'NorthEast',
    ENE: 'East-Northeast',
    E: 'East',
    ESE: 'East-SouthEast',
    SE: 'SouthEast',
    SSE: 'South-SouthEast',
    S: 'South',
    SSW: 'South-SouthWest',
    SW: 'SouthWest',
    WSW: 'West-SouthWest',
    W: 'West',
    WNW: 'West-NorthWest',
    NW: 'NorthWest',
    NNW: 'North-NorthWest',
    // Add more directions as needed
};

const getDirection = (abbr) => {
    return directionMapping[abbr] || abbr; // Return full direction or abbreviation if not found
};
    return (
        <div className="p-4 bg-white shadow-md rounded-lg">
            <h2 className="text-xl font-semibold">Wind Speed and Direction</h2>
            <p>Speed: {data.wind_kph} km/h</p>
            <p>Direction: {getDirection(data.wind_dir)}</p>
        </div>
    );
};

export default WindSpeedDirection