import React from 'react';

const WeatherForecast = ({ data }) => {
    return (
        <div className="p-4 bg-white shadow-md rounded-lg">
            <h2 className="text-xl font-semibold">5-Day Forecast</h2>
            <div className="flex justify-between">
                {data.map((day, index) => (
                    <div key={index} className="text-center">
                        <p>{day.date}</p>
                        <p>{day.day.maxtemp_c}°C / {day.day.mintemp_c}°C</p>
                        <p>{day.day.condition.text}</p>
                        <img src={day.day.condition.icon} alt={day.day.condition.text} />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default WeatherForecast;
