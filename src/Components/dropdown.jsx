import React from "react";
import { Link } from "react-router-dom";

function Dropdown(props){

    return(
        <>
        <div className="bg-red-950">
            <div className="headerbottom container m-auto gap-5 hidden md:flex">
                <div className="dropdown inline-block">
                    <button className="text-md text-red-200 w-200 px-5 py-3 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-200">Happening Now</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Regional</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Politics</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">IT</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Education</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Foreign Affairs</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Sports</Link>
                    </div>
                </div>
                <div className="dropdown inline-block">
                    <button className="text-md text-red-200 px-5 py-3 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-200">Exclusive</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Politics</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">IT</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Sports</Link>
                    </div>
                </div>
                <div className="dropdown inline-block">
                    <button className="text-md text-red-200 px-5 py-3 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-200">Technology</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Tech Blogs</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">New Releases</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Latest Gadgets</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Space Technology</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">New Discoveries</Link>
                    </div>
                </div>
                <div className="dropdown inline-block">
                    <button className="text-md text-red-200 px-5 py-3 border-0 cursor-pointer bg-red-950 hover:bg-red-400 hover:text-red-950 ease-in-out duration-200">Sports</button>
                    <div className="content drop-shadow-2xl hidden absolute min-w-28 text-red-950 bg-red-400">
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Cricket</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Football</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Women's Cricket</Link>
                        <Link className="block px-5 py-3 text-md bg-red-200 text-red-950 hover:text-red-200 hover:bg-red-950 ease-in-out duration-200" to="#">Badminton</Link>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}
export default Dropdown