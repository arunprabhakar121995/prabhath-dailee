import React from 'react';

const CurrentDate = () => {
    const now = new Date();
    const options = { 
        weekday: 'long', 
        year: 'numeric', 
        month: 'long', 
        day: 'numeric' 
    };
    const currentDate = now.toLocaleDateString('en-US', options);

    return (
        <div id="current-date" className='text-sm font-bold mx-2 my-2'>
            {currentDate}
        </div>
    );
};

export default CurrentDate;