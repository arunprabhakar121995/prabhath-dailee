import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "/node_modules/flag-icons/css/flag-icons.min.css";
import './index.css'
import Root from "./Routes/root";
import ErrorPage from './error-page';
import Home, { loader as HomeLoader } from './Routes/home';
import Articles, { loader as ArticlesLoader} from './Routes/Article/articles';
import Article, {loader as ArticleLoader} from './Routes/Article/article';
import Authors, { loader as AuthorsLoader} from './Routes/Author/authors';
import Author, { loader as AuthorLoader} from './Routes/Author/author';
import WeatherDetails from './Routes/weatherDetails';
import WorldMap from './Routes/worldMap';
import Login from './Routes/AuthPages/login';
import SignUp from './Routes/AuthPages/signup';
import store from './app/store';
import { Provider } from 'react-redux';
import Logout from './Routes/AuthPages/logout';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
        loader: HomeLoader,
      },
      {
        path: "/articles",
        element: <Articles />,
        loader: ArticlesLoader,
      },
      {
        path: "articles/:articleId",
        element: <Article />,
        loader: ArticleLoader,
      },
      {
        path: "/authors",
        element: <Authors />,
        loader: AuthorsLoader,
      },
      {
        path: "/authors/:authorId",
        element: <Author />,
        loader: AuthorLoader,
      },
      {
        path: "/weather",
        element: <WeatherDetails />
      },
      {
        path: "/worldmap",
        element: <WorldMap />
      },
      {
        path: "/login",
        element: <Login />
      },
      {
        path: "/signup",
        element: <SignUp />
      },
      {
        path: "/logout",
        element: <Logout />
      }
    ]
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
)
